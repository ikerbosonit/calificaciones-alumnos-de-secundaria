
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author iker.garcia
 */
public class Vista extends javax.swing.JFrame {

    /**
     * 
     * Creates new form Vista
     */
    //creo variables **************************************************
    
    double Id = 0;
    String Nombre = "";
    String Apellido = "";
    double Matematicas = 0;
    double Fisica = 0;
    double Lengua = 0;
    double Ingles = 0;
    double Historia = 0;
    double MediaP = 0;
    double MediaA = 0;
    double Maxima = 0;
    double Minima = 0;

    List<Persona> lista = new ArrayList<Persona>();

    public Vista() {
        initComponents();
        jTablePersona.addMouseListener(new MouseAdapter() {
            DefaultTableModel model = new DefaultTableModel();

            public void mouseClicked(MouseEvent e) {
                int i = jTablePersona.getSelectedRow();
                Id = Double.parseDouble(jTablePersona.getValueAt(i, 0).toString());
                Nombre = (jTablePersona.getValueAt(i, 1).toString());
                Apellido = (jTablePersona.getValueAt(i, 2).toString());
                Matematicas = Double.parseDouble(jTablePersona.getValueAt(i, 3).toString());
                Fisica = Double.parseDouble(jTablePersona.getValueAt(i, 4).toString());
                Lengua = Double.parseDouble(jTablePersona.getValueAt(i, 5).toString());
                Ingles = Double.parseDouble(jTablePersona.getValueAt(i, 6).toString());
                Historia = Double.parseDouble(jTablePersona.getValueAt(i, 7).toString());
                MediaP = Double.parseDouble(jTablePersona.getValueAt(i, 8).toString());
                MediaA = Double.parseDouble(jTablePersona.getValueAt(i, 9).toString());
                Maxima = Double.parseDouble(jTablePersona.getValueAt(i, 10).toString());
                Minima = Double.parseDouble(jTablePersona.getValueAt(i, 11).toString());
            }
        });
    }

    /**
     * Este método se llama desde el constructor para inicializar el formulario.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        textId = new javax.swing.JLabel();
        textNombre = new javax.swing.JLabel();
        textApellido = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePersona =  new javax.swing.JTable();//new Colores();//new javax.swing.JTable();//new ux.Colors();
        txtEditarMatematicas = new javax.swing.JTextField();
        txtEditarNombre = new javax.swing.JTextField();
        txtEditarFisica = new javax.swing.JTextField();
        txtEditarLengua = new javax.swing.JTextField();
        txtEditarApellido = new javax.swing.JTextField();
        txtEditarId = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtEditarIngles = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtEditarHistoria = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        txtMatematicas = new javax.swing.JTextField();
        txtFisica = new javax.swing.JTextField();
        txtLengua = new javax.swing.JTextField();
        txtIngles = new javax.swing.JTextField();
        txtHistoria = new javax.swing.JTextField();
        textHistoria = new javax.swing.JLabel();
        textIngles = new javax.swing.JLabel();
        textLengua = new javax.swing.JLabel();
        textFisica = new javax.swing.JLabel();
        textMatematicas = new javax.swing.JLabel();
        BtnRegistrar = new javax.swing.JButton();
        textApellido2 = new javax.swing.JLabel();
        BtnEliminar = new javax.swing.JToggleButton();
        BtnEditar = new javax.swing.JToggleButton();
        textApellido1 = new javax.swing.JLabel();
        BtnTerminarEdicion = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 0));
        jLabel1.setText("Programa de calificaciones");

        textId.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textId.setText("Id");

        textNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textNombre.setText("Nombre");

        textApellido.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textApellido.setText("Apellido");

        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });

        jFormattedTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jFormattedTextField1KeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textId, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtApellido))
                .addGap(0, 104, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textId, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jTablePersona.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nombre", "Apellido", "Matematicas", "Fisica", "Lengua", "Ingles", "Historia", "MediaP", "MediaA", "Maxima", "Minima"
            }
        ));
        jTablePersona.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTablePersona.setRowHeight(30);
        jScrollPane2.setViewportView(jTablePersona);

        txtEditarMatematicas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarMatematicasKeyTyped(evt);
            }
        });

        txtEditarNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarNombreKeyTyped(evt);
            }
        });

        txtEditarFisica.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarFisicaKeyTyped(evt);
            }
        });

        txtEditarLengua.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarLenguaKeyTyped(evt);
            }
        });

        txtEditarApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarApellidoKeyTyped(evt);
            }
        });

        txtEditarId.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtEditarId.setForeground(new java.awt.Color(51, 51, 255));
        txtEditarId.setText("Id");

        jLabel3.setText("Nombre");

        jLabel4.setText("Apeliido");

        jLabel5.setText("Matematicas");

        jLabel8.setText("Fisica");

        jLabel9.setText("Ingles");

        jLabel10.setText("Lengua");

        txtEditarIngles.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarInglesKeyTyped(evt);
            }
        });

        jLabel13.setText("Historia");

        txtEditarHistoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEditarHistoriaKeyTyped(evt);
            }
        });

        txtMatematicas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMatematicasKeyTyped(evt);
            }
        });

        txtFisica.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFisicaKeyTyped(evt);
            }
        });

        txtLengua.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLenguaKeyTyped(evt);
            }
        });

        txtIngles.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtInglesKeyTyped(evt);
            }
        });

        txtHistoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtHistoriaKeyTyped(evt);
            }
        });

        textHistoria.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textHistoria.setText("Historia");

        textIngles.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textIngles.setText("Ingles");

        textLengua.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textLengua.setText("Lengua");

        textFisica.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textFisica.setText("Fisica");

        textMatematicas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        textMatematicas.setText("Matemáticas");

        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        textApellido2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        textApellido2.setForeground(new java.awt.Color(0, 0, 204));
        textApellido2.setText("Calificaciones");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(textHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(textMatematicas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtMatematicas, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(textFisica, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtFisica, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(textLengua, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtLengua, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(textIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnRegistrar)
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(textApellido2)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(textApellido2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textMatematicas, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMatematicas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFisica, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFisica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textLengua, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLengua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addComponent(BtnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        BtnEliminar.setText("Eliminar");
        BtnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEliminarActionPerformed(evt);
            }
        });

        BtnEditar.setText("Editar");
        BtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditarActionPerformed(evt);
            }
        });

        textApellido1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        textApellido1.setForeground(new java.awt.Color(0, 0, 204));
        textApellido1.setText("Alumnos");

        BtnTerminarEdicion.setText("Confirmar Edicion");
        BtnTerminarEdicion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTerminarEdicionActionPerformed(evt);
            }
        });

        jLabel6.setText("Id_Alumno");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(99, 99, 99)
                        .addComponent(textApellido1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 51, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtEditarId, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(139, 139, 139)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(BtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(BtnTerminarEdicion, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtEditarApellido, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                                            .addComponent(txtEditarNombre))))
                                .addGap(6, 6, 6)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(jLabel8))
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel13)
                                .addGap(15, 15, 15)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtEditarHistoria)
                            .addComponent(txtEditarMatematicas)
                            .addComponent(txtEditarFisica)
                            .addComponent(txtEditarLengua)
                            .addComponent(txtEditarIngles, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(175, 175, 175)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jScrollPane2))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(230, 230, 230)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(11, 11, 11))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(BtnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(textApellido1)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(109, 109, 109)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(BtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtEditarNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtEditarApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtEditarId)))
                        .addGap(28, 28, 28)
                        .addComponent(BtnTerminarEdicion, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEditarMatematicas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEditarFisica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEditarLengua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEditarIngles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(txtEditarHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(39, 39, 39))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

//BOTÓN  DE REGISTRAR LA INFORMACION
//---------------------------------------
    /**
     * Parte 1 ---------- El Boton de registrar analizará en el momento de
     * ejectuarlo que los valores metidos estén entre 1 y 10 para cada
     * asignatura.
     * 
     * Se hará un anidado desde el if inicial para que finalice con
     * la condición de que si no cumple todas las condiciones anteriores,
     * rellenara el cuadro con las notas.
     *
     * Parte 2 ---------- Si el resultado del calculo de la media ponderada es
     * inferior a 5, significa que es un estudiante que no llega al aprobado y
     * por lo tanto, se refleja coloreando la casilla en rojo y los numero en
     * amarillo.
     * En caso contrario, se coloreará de azul y blanco.
     *
     * @param evt
     */
    private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed
       try {
// Matematicas
        //------------
        if (Double.parseDouble(txtMatematicas.getText()) < 1 || Double.parseDouble(txtMatematicas.getText()) > 10) {
            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
            JOptionPane.showMessageDialog(frame,
                    "Las calificaciones han de ser entre 1 y 10",
                    "WARNING_MESSAGE",
                    JOptionPane.WARNING_MESSAGE);
        } else {

// Fisica
            //---------
            if (Double.parseDouble(txtFisica.getText()) < 1 || Double.parseDouble(txtFisica.getText()) > 10) {
                JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                JOptionPane.showMessageDialog(frame,
                        "Las calificaciones han de ser entre 1 y 10",
                        "WARNING_MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
            } else {

// Lengua
                //--------
                if (Double.parseDouble(txtLengua.getText()) < 1 || Double.parseDouble(txtLengua.getText()) > 10) {
                    JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                    JOptionPane.showMessageDialog(frame,
                            "Las calificaciones han de ser entre 1 y 10",
                            "WARNING_MESSAGE",
                            JOptionPane.WARNING_MESSAGE);
                } else {

// Ingles
                    //---------
                    if (Double.parseDouble(txtIngles.getText()) < 1 || Double.parseDouble(txtIngles.getText()) > 10) {
                        JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                        JOptionPane.showMessageDialog(frame,
                                "Las calificaciones han de ser entre 1 y 10",
                                "WARNING_MESSAGE",
                                JOptionPane.WARNING_MESSAGE);
                    } else {

// Historia
                        //---------
                        if (Double.parseDouble(txtHistoria.getText()) < 1 || Double.parseDouble(txtHistoria.getText()) > 10) {
                            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                            JOptionPane.showMessageDialog(frame,
                                    "Las calificaciones han de ser entre 1 y 10",
                                    "WARNING_MESSAGE",
                                    JOptionPane.WARNING_MESSAGE);
                        } else {

// Si no se cumplen ninguna de las anteriores, se rellena el cuadro
                            //-----------------------------------------------------------------
                            Persona persona = new Persona(Double.parseDouble(jFormattedTextField1.getText()),
                                    txtNombre.getText(),
                                    txtApellido.getText(),
                                    Double.parseDouble(txtMatematicas.getText()),
                                    Double.parseDouble(txtFisica.getText()),
                                    Double.parseDouble(txtLengua.getText()),
                                    Double.parseDouble(txtIngles.getText()),
                                    Double.parseDouble(txtHistoria.getText())
                            );
                            lista.add(persona);

                            mostrar();

//-----------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------
                            jTablePersona.setDefaultRenderer(Object.class, new TableCellRenderer() {
                                private DefaultTableCellRenderer DEFAULT_RENDERER = new DefaultTableCellRenderer();

                                @Override
                                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                                    Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                                                    int align = DefaultTableCellRenderer.CENTER;
                                                    ((DefaultTableCellRenderer) c ).setHorizontalAlignment(align);
                                                    c.setBackground(Color.WHITE);
                                                    c.setForeground(Color.BLACK);

//Se establece la condición de que si la nota calculada es menor que 5 se definen colores para los numeros y el fondo, por el contrario,
//si son superiores a 5 tendran otros colores.                                       
//-------
//MediaP
//---------------------------------------------------------------------------------------------------------------------
                                    if (table.getColumnModel().getColumn(column).getIdentifier().equals("MediaP")) {
                                        if (Double.parseDouble(value.toString()) < 5) {
                                            c.setBackground(Color.RED);
                                            c.setForeground(Color.YELLOW);
                                            Font font = new Font("TimesRoman", Font.BOLD, 17);
                                            c.setFont(font);

                                        } else {
                                            c.setBackground(Color.BLUE);
                                            c.setForeground(Color.white);
                                            Font font = new Font("TimesRoman", Font.BOLD, 17);
                                            c.setFont(font);
                                        }
                                    }
//MediaA
//---------------------------------------------------------------------------------------------------------------------                            
                                    if (table.getColumnModel().getColumn(column).getIdentifier().equals("MediaA")) {
                                        if (Double.parseDouble(value.toString()) < 5) {
                                            c.setBackground(Color.PINK);
                                            c.setForeground(Color.RED);
                                            Font font = new Font("TimesRoman", Font.BOLD, 17);
                                            c.setFont(font);
                                        } else {
                                            c.setBackground(Color.CYAN);
                                            c.setForeground(Color.blue);
                                            Font font = new Font("TimesRoman", Font.BOLD, 17);
                                            c.setFont(font);
                                        }
                                    }
                                  
//Colores para Maximos  y Minimos, aplicamos la misma norma coloreando los numero en azul aquellos que sean aprobados, y en rojo
//aquellos que sean suspenso.

                                    //MAXIMA
                                    
                                    if (table.getColumnModel().getColumn(column).getIdentifier().equals("Maxima")) {
                                        if (Double.parseDouble(value.toString()) < 5) {
                                            c.setForeground(Color.RED);
                                        } else {
                                            c.setForeground(Color.BLUE);
                                        }
                                    }

                                    //MINIMA
                                    if (table.getColumnModel().getColumn(column).getIdentifier().equals("Minima")) {
                                        if (Double.parseDouble(value.toString()) < 5) {
                                            c.setForeground(Color.RED);
                                        } else {
                                            c.setForeground(Color.BLUE);
                                        }
                                    }
                                    return c;
                                    
                                }

                            }
                            );
                        }
                    }
                }
            };


    }//GEN-LAST:event_BtnRegistrarActionPerformed
       
 } catch (NumberFormatException e) {
            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
            JOptionPane.showMessageDialog(frame, "Campos vacios", "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
        }

    }
    /**
     * Se aplica un botón para poder hacer el borrado de la línea deseada.
     * Se usa un bucle for que recorre todas las celdas de la fila
     * @param evt 
     */
    private void BtnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEliminarActionPerformed

        for (int i = 0; i < lista.size(); i++) {
            if (Id == lista.get(i).getId()) {
                lista.remove(i);
            }
        }
        
        //actualizar en el borrado, esto es que pueda borrar el elemento seleccionado
        //----------------------------------------------------------------------------
        String matriz[][] = new String[lista.size()][12];
        for (int i = 0; i < lista.size(); i++) {

            matriz[i][0] = String.valueOf(lista.get(i).getId());
            matriz[i][1] = lista.get(i).getNombre();
            matriz[i][2] = lista.get(i).getApellido();
            matriz[i][3] = String.valueOf(lista.get(i).getMatematicas());
            matriz[i][4] = String.valueOf(lista.get(i).getFisica());
            matriz[i][5] = String.valueOf(lista.get(i).getLengua());
            matriz[i][6] = String.valueOf(lista.get(i).getIngles());
            matriz[i][7] = String.valueOf(lista.get(i).getHistoria());
            matriz[i][8] = String.valueOf(lista.get(i).calcularMediaP());
            matriz[i][9] = String.valueOf(lista.get(i).getmediaA());
            matriz[i][10] = String.valueOf(lista.get(i).calcularMax());
            matriz[i][11] = String.valueOf(lista.get(i).calcularMin());
        }
        
            
        
        jTablePersona.setModel(new javax.swing.table.DefaultTableModel(
                matriz,
                new String[]{
                    "Id", "Nombre", "Apellido", "Matematicas", "Fisica", "Lengua", "Inglés", "Historia", "MediaP", "MediaA", "Maxima", "Minima"
                }
        ));

    }//GEN-LAST:event_BtnEliminarActionPerformed

    private void BtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditarActionPerformed
        
        txtEditarNombre.setText(Nombre);
        txtEditarApellido.setText(Apellido);
        txtEditarMatematicas.setText(Double.toString(Matematicas));
        txtEditarFisica.setText(Double.toString(Fisica));
        txtEditarLengua.setText(Double.toString(Lengua));
        txtEditarIngles.setText(Double.toString(Ingles));
        txtEditarHistoria.setText(Double.toString(Historia));
        txtEditarId.setText(Double.toString(Id));
    }//GEN-LAST:event_BtnEditarActionPerformed

    private void BtnTerminarEdicionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTerminarEdicionActionPerformed
        try {
        for (int i = 0; i < lista.size(); i++) {

            if (Double.parseDouble(txtEditarId.getText()) == lista.get(i).getId()) {
                lista.get(i).setNombre(txtEditarNombre.getText());
                lista.get(i).setApellido(txtEditarApellido.getText());
                lista.get(i).setMatematicas(Double.parseDouble(txtEditarMatematicas.getText()));
                lista.get(i).setFisica(Double.parseDouble(txtEditarFisica.getText()));
                lista.get(i).setLengua(Double.parseDouble(txtEditarLengua.getText()));
                lista.get(i).setIngles(Double.parseDouble(txtEditarIngles.getText()));
                lista.get(i).setHistoria(Double.parseDouble(txtEditarHistoria.getText()));

            }

        }
        
            // Matematicas
            //------------
            if (Double.parseDouble(txtEditarMatematicas.getText()) < 1 || Double.parseDouble(txtEditarMatematicas.getText()) > 10) {
                JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                JOptionPane.showMessageDialog(frame,
                        "Las calificaciones han de ser entre 1 y 10",
                        "WARNING_MESSAGE",
                        JOptionPane.WARNING_MESSAGE);
            } else {

// Fisica
                //---------
                if (Double.parseDouble(txtEditarFisica.getText()) < 1 || Double.parseDouble(txtEditarFisica.getText()) > 10) {
                    JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                    JOptionPane.showMessageDialog(frame,
                            "Las calificaciones han de ser entre 1 y 10",
                            "WARNING_MESSAGE",
                            JOptionPane.WARNING_MESSAGE);
                } else {

// Lengua
                    //--------
                    if (Double.parseDouble(txtEditarLengua.getText()) < 1 || Double.parseDouble(txtEditarLengua.getText()) > 10) {
                        JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                        JOptionPane.showMessageDialog(frame,
                                "Las calificaciones han de ser entre 1 y 10",
                                "WARNING_MESSAGE",
                                JOptionPane.WARNING_MESSAGE);
                    } else {

// Ingles
                        //---------
                        if (Double.parseDouble(txtEditarIngles.getText()) < 1 || Double.parseDouble(txtEditarIngles.getText()) > 10) {
                            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                            JOptionPane.showMessageDialog(frame,
                                    "Las calificaciones han de ser entre 1 y 10",
                                    "WARNING_MESSAGE",
                                    JOptionPane.WARNING_MESSAGE);
                        } else {

// Historia
                            //---------
                            if (Double.parseDouble(txtEditarHistoria.getText()) < 1 || Double.parseDouble(txtEditarHistoria.getText()) > 10) {
                                JFrame frame = new JFrame("JOptionPane showMessageDialog example");
                                JOptionPane.showMessageDialog(frame,
                                        "Las calificaciones han de ser entre 1 y 10",
                                        "WARNING_MESSAGE",
                                        JOptionPane.WARNING_MESSAGE);
                            } else {
                                //txtEditarId.setText("");
                                txtEditarNombre.setText("");
                                txtEditarApellido.setText("");
                                txtEditarMatematicas.setText("");
                                txtEditarFisica.setText("");
                                txtEditarLengua.setText("");
                                txtEditarIngles.setText("");
                                txtEditarHistoria.setText("");

                                String matriz[][] = new String[lista.size()][12];

                                for (int i = 0; i < lista.size(); i++) {

                                    matriz[i][0] = String.valueOf(lista.get(i).getId());
                                    matriz[i][1] = lista.get(i).getNombre();
                                    matriz[i][2] = lista.get(i).getApellido();
                                    matriz[i][3] = String.valueOf(lista.get(i).getMatematicas());
                                    matriz[i][4] = String.valueOf(lista.get(i).getFisica());
                                    matriz[i][5] = String.valueOf(lista.get(i).getLengua());
                                    matriz[i][6] = String.valueOf(lista.get(i).getIngles());
                                    matriz[i][7] = String.valueOf(lista.get(i).getHistoria());
                                    matriz[i][8] = String.valueOf(lista.get(i).calcularMediaP());
                                    matriz[i][9] = String.valueOf(lista.get(i).getEditedMediaA());
                                    matriz[i][10] = String.valueOf(lista.get(i).calcularMax());
                                    matriz[i][11] = String.valueOf(lista.get(i).calcularMin());
                                }

                                jTablePersona.setModel(new javax.swing.table.DefaultTableModel(
                                        matriz,
                                        new String[]{
                                            "Id", "Nombre", "Apellido", "Matematicas", "Fisica", "Lengua", "Ingles", "Historia", "MediaP", "MediaA", "Maxima", "Minima"
                                        }
                                ));
                            }
                        }
                    }
                }
            }

        } catch (NumberFormatException e) {
            JFrame frame = new JFrame("JOptionPane showMessageDialog example");
            JOptionPane.showMessageDialog(frame, "Campos vacios", "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
        }
        
    }//GEN-LAST:event_BtnTerminarEdicionActionPerformed


    //Según el tipo de dato que introduzcamos, se asegurará cada casilla que cada dato sea
    // del tipo que corresponda
    //------------------------------------------------------------
    //-----------------------------------------------------------------------------------------Nombre
    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        // TODO add your handling code here:
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Ingresar solo caracteres");
        }
    }//GEN-LAST:event_txtNombreKeyTyped
//-----------------------------------------------------------------------------------------------ID
    /**
     * Se establece el formato con el cual ha de rellenarse el cuadro.
     * @param evt 
     */
    private void jFormattedTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedTextField1KeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_jFormattedTextField1KeyTyped
//-----------------------------------------------------------------------------------------------Apellido
    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo caracteres");
        }
    }//GEN-LAST:event_txtApellidoKeyTyped
//-----------------------------------------------------------------------------------------------Matematicas
    private void txtMatematicasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMatematicasKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_txtMatematicasKeyTyped
//-----------------------------------------------------------------------------------------------Fisica
    private void txtFisicaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFisicaKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_txtFisicaKeyTyped
//-----------------------------------------------------------------------------------------------Lengua
    private void txtLenguaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLenguaKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_txtLenguaKeyTyped
//-----------------------------------------------------------------------------------------------Inglés
    private void txtInglesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInglesKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_txtInglesKeyTyped
//-----------------------------------------------------------------------------------------------Historia
    private void txtHistoriaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHistoriaKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo numeros");
        }
    }//GEN-LAST:event_txtHistoriaKeyTyped


    /**
     * Se establece el formato con el que han de de rellenarse las casillas de edicion.
     * @param evt 
     */
    private void txtEditarMatematicasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarMatematicasKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACK_SPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtEditarMatematicasKeyTyped

    private void txtEditarFisicaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarFisicaKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACK_SPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtEditarFisicaKeyTyped

    private void txtEditarLenguaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarLenguaKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACK_SPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtEditarLenguaKeyTyped

    private void txtEditarInglesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarInglesKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACK_SPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtEditarInglesKeyTyped

    private void txtEditarHistoriaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarHistoriaKeyTyped
        char vChar = evt.getKeyChar();
        if (!(Character.isDigit(vChar)
                || (vChar == KeyEvent.VK_BACK_SPACE)
                || (vChar == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
    }//GEN-LAST:event_txtEditarHistoriaKeyTyped

    
    private void txtEditarNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarNombreKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo caracteres");
        }
    }//GEN-LAST:event_txtEditarNombreKeyTyped

    private void txtEditarApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEditarApellidoKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();

            JOptionPane.showMessageDialog(rootPane, "Ingresar solo caracteres");
        }
    }//GEN-LAST:event_txtEditarApellidoKeyTyped
    //mostrar en la tabla----------------------------------------------------------------
    public void mostrar() {
        String matriz[][] = new String[lista.size()][12];

        for (int i = 0; i < lista.size(); i++) {

            matriz[i][0] = String.valueOf(lista.get(i).getId());
            matriz[i][1] = lista.get(i).getNombre();
            matriz[i][2] = lista.get(i).getApellido();
            matriz[i][3] = String.valueOf(lista.get(i).getMatematicas());
            matriz[i][4] = String.valueOf(lista.get(i).getFisica());
            matriz[i][5] = String.valueOf(lista.get(i).getLengua());
            matriz[i][6] = String.valueOf(lista.get(i).getIngles());
            matriz[i][7] = String.valueOf(lista.get(i).getHistoria());
            matriz[i][8] = String.valueOf(lista.get(i).calcularMediaP());
            matriz[i][9] = String.valueOf(lista.get(i).getmediaA());
            matriz[i][10] = String.valueOf(lista.get(i).calcularMax());
            matriz[i][11] = String.valueOf(lista.get(i).calcularMin());
        }
        jTablePersona.setModel(new javax.swing.table.DefaultTableModel(
                matriz,
                new String[]{
                    "Id", "Nombre", "Apellido", "Matematicas", "Fisica", "Lengua", "Inglés", "Historia", "MediaP", "MediaA", "Maxima", "Minima"
                }
        ));

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton BtnEditar;
    private javax.swing.JToggleButton BtnEliminar;
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton BtnTerminarEdicion;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTablePersona;
    private javax.swing.JLabel textApellido;
    private javax.swing.JLabel textApellido1;
    private javax.swing.JLabel textApellido2;
    private javax.swing.JLabel textFisica;
    private javax.swing.JLabel textHistoria;
    private javax.swing.JLabel textId;
    private javax.swing.JLabel textIngles;
    private javax.swing.JLabel textLengua;
    private javax.swing.JLabel textMatematicas;
    private javax.swing.JLabel textNombre;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtEditarApellido;
    private javax.swing.JTextField txtEditarFisica;
    private javax.swing.JTextField txtEditarHistoria;
    private javax.swing.JLabel txtEditarId;
    private javax.swing.JTextField txtEditarIngles;
    private javax.swing.JTextField txtEditarLengua;
    private javax.swing.JTextField txtEditarMatematicas;
    private javax.swing.JTextField txtEditarNombre;
    private javax.swing.JTextField txtFisica;
    private javax.swing.JTextField txtHistoria;
    private javax.swing.JTextField txtIngles;
    private javax.swing.JTextField txtLengua;
    private javax.swing.JTextField txtMatematicas;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
