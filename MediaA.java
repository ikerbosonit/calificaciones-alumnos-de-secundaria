


import java.text.ParseException;


/**
 *Se crea el hilo con la clase  de Media Aritmetica como MediaA
 * @author iker.garcia
 */

public class MediaA extends Thread {
    double matematicas,fisica,lengua,ingles,historia;
    double Resultado;
    /**
     * Constructor publico y no devuelve nada.
     * Se crea la clase MediaA como media aritmetica que se calculara usando un hilo con los siguientes parametros
     * @param matematicas Recibe un dato numerico double como la calificacion en matematicas
     * @param fisica Recibe un dato numerico double como la calificacion en fisica
     * @param lengua Recibe un dato numerico double como la calificacion en lengua
     * @param ingles Recibe un dato numerico double como la calificacion en ingles
     * @param historia Recibe un dato numerico double como la calificacion en historia
     */
    public MediaA(double matematicas, double fisica, double lengua, double ingles, double historia) {
        this.matematicas = matematicas;
        this.fisica = fisica;
        this.lengua = lengua;
        this.ingles = ingles;
        this.historia = historia;
        run();
    }

    
    /**
     * Se crea un hilo que calcule la media aritmetica
     * La media aritmetica se calculará en fución del numero de asignaturas, que es un
     * valor fijo, en este caso, es un valor  = 5. 
     * Resultado sera la suma de cada asignatura , dividido entre el numero de asignaturas.
     * @run
     */
    @Override
    public void run() {
        try{
        Resultado = (( this.matematicas+
                this.lengua+
                this.ingles+
                this.historia+
                this.fisica)/5);
        }catch(NumberFormatException e){
            System.out.println("Error, ha de ser un numero");
            
        }
    }

    public double getResultado() {
        return Resultado;   
    }
    /**
     * MediaA = Devuelve el resultado del calculo de la media aritmetica
     * @return
     */
}
