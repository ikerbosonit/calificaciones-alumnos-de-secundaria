
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * La clase persona va a representar todos los atributos del alumno, con sus
 * asignaturas. Se usarán atributos públicos. Aquellos que sean caracter seran
 * tipo 'String' y aquellos que reciban números, serán tipo 'double'.
 *
 * @author iker.garcia
 */
public class Persona {

    String Nombre, Apellido;
    double Id, Matematicas, Fisica, Lengua, Ingles, Historia, MediaP, MediaA, Maxima, Minima;

    /**
     * Se creará una clase Persona, recibiendo como parámetros los siguientes;
     *
     * @param Id Identificara numericamente a cada Alumno
     * @param Nombre Se introduce como caracter el nombre del alumno
     * @param Apellido Se introduce como caracter el apellido del alumno
     * @param Matematicas Se introduce como double la calificacion en matematicas
     * @param Fisica Se introduce como double la calificacion en Fisica
     * @param Lengua Se introduce como double la calificacion en Lengua
     * @param Ingles Se introduce como double la calificacion en Ingles
     * @param Historia Se introduce como double la calificacion en Historia
     */
    //Esto es lo que viene del formulario
    public Persona(double Id, String Nombre, String Apellido,
            double Matematicas, double Fisica, double Lengua, double Ingles, double Historia) {
        /**
         * creamos el objeto mediaA de tipo MediaA. Para construir el objeto,
         * usamos new para llamar al constructor de la clase
        *
         */
        MediaA mediaA = new MediaA(Matematicas, Fisica, Lengua, Ingles, Historia);
        //mediaA.start();
        //Constructor = Inicialización de los atributos          
        this.Id = Id;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Matematicas = Matematicas;
        this.Fisica = Fisica;
        this.Lengua = Lengua;
        this.Ingles = Ingles;
        this.Historia = Historia;
        this.MediaP = calcularMediaP();
        this.MediaA = mediaA.getResultado();
        this.Maxima = calcularMax();
        this.Minima = calcularMin();
    }

    /**
     * Recibe el valor del parametro de ID
     *
     * @return
     */
    //---------------------------------------ID
    public double getId() {
        return Id;
    }

    /**
     * Devuelve el valor del parametro ID
     *
     * @param Id
     */
    public void setId(double Id) {
        this.Id = Id;
    }

    //---------------------------------------NOMBRE
    /**
     * Recibe el valor del parametro de NOMBRE
     *
     * @return
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * Devuelve el valor introducido de NOMBRE
     *
     * @param Nombre
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    //---------------------------------------APELLLIDO
    /**
     * Recibe el valor del parametro de APELLLIDO
     *
     * @return
     */
    public String getApellido() {
        return Apellido;
    }

    /**
     * Devuelve el valor introducido de APELLLIDO
     *
     * @param Apellido
     */
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    //---------------------------------------MATEMATICAS
    /**
     * Recibe el valor del parametro de MATEMATICAS
     *
     * @return
     */
    public double getMatematicas() {
        return Matematicas;
    }

    /**
     * Devuelve el valor introducido de MATEMATICAS
     *
     * @param Matematicas
     */
    public void setMatematicas(double Matematicas) {
        this.Matematicas = Matematicas;
    }

    //---------------------------------------FISICA
    /**
     * Recibe el valor del parametro de FISICA
     *
     * @return
     */
    public double getFisica() {
        return Fisica;
    }

    /**
     * Devuelve el valor introducido de FISICA
     *
     * @param Fisica
     */
    public void setFisica(double Fisica) {
        this.Fisica = Fisica;
    }

    //---------------------------------------LENGUA
    /**
     * Recibe el valor del parametro de LENGUA
     *
     * @return
     */
    public double getLengua() {
        return Lengua;
    }

    /**
     * Devuelve el valor introducido de LENGUA
     *
     * @param Lengua
     */
    public void setLengua(double Lengua) {
        this.Lengua = Lengua;
    }

    //---------------------------------------INGLES
    /**
     * Recibe el valor del parametro de INGLES
     *
     * @return
     */
    public double getIngles() {
        return Ingles;
    }

    /**
     * Devuelve el valor introducido de INGLES
     *
     * @param Ingles
     */
    public void setIngles(double Ingles) {
        this.Ingles = Ingles;
    }

    //---------------------------------------HISTORIA
    /**
     * Recibe el valor del parametro de HISTORIA
     *
     * @return
     */
    public double getHistoria() {
        return Historia;
    }

    /**
     * Devuelve el valor introducido de HISTORIA
     *
     * @param Historia
     */
    public void setHistoria(double Historia) {
        this.Historia = Historia;
    }

    //---------------------------------------------calculo la media Ponderada
    /**
     * Devuelve el valor calculado de la media ponderada
     *
     * @return
     */
    public double getMediaP() {
        return MediaP;
    }

    /**
     * Calcula la media ponderada
     *
     * @return
     */
    public double calcularMediaP() {
        double suma = 0;
        suma = suma + this.Matematicas * (0.35f);
        suma = suma + this.Lengua * (0.10f);
        suma = suma + this.Ingles * (0.25f);
        suma = suma + this.Historia * (0.05f);
        suma = suma + this.Fisica * (0.25f);
        BigDecimal mp = new BigDecimal(suma).setScale(2, RoundingMode.HALF_UP);
        return mp.doubleValue();
    }

    //----------------------------------------------calculo el maximo
    /**
     * Recibe el valor maximo de notas
     *
     * @return
     *
     */
    public double getMax() {
        return Maxima;
    }

    /**
     * Calcula la nota maxima
     *
     * @return
     */
    public double calcularMax() {
        double max = -1;
        if (max < this.Matematicas) {
            max = this.Matematicas;
        }

        if (max < this.Lengua) {
            max = (this.Lengua);
        }

        if (max < this.Ingles) {
            max = this.Ingles;
        }

        if (max < this.Historia) {
            max = this.Historia;
        }

        if (max < this.Fisica) {
            max = this.Fisica;
        }

        return max;
    }

    //------------------------------------------------calculo el minimo
    /**
     * Recibe la nota minima
     *
     * @return
     */
    public double getMin() {
        return Minima;
    }

    /**
     * Calcula la nota minima.
     *
     * @return
     */
    public double calcularMin() {
        double min = 11;
        if (min > this.Matematicas) {
            min = this.Matematicas;
        }
        if (min > this.Lengua) {
            min = this.Lengua;
        }
        if (min > this.Ingles) {
            min = this.Ingles;
        }
        if (min > this.Historia) {
            min = this.Historia;
        }
        if (min > this.Fisica) {
            min = this.Fisica;
        }
        return min;

    }

    /**
     * Recibe la nota de media aritmetica
     *
     * @return
     */

    public double getmediaA() {
        return this.MediaA;

    }

    /**
     * Recibe la nota de media aritmetica
     * @return 
     */
    public double getEditedMediaA() {
        MediaA mediaA = new MediaA(Matematicas, Fisica, Lengua, Ingles, Historia);
        return mediaA.getResultado();
    }

    /**
     * Devuelve el calculo de la media aritmetica
     * @param mediaA 
     */
    public void setMediaA(double mediaA) {
        this.MediaA = mediaA;
    }
    
}
