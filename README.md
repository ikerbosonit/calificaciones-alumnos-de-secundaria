### Aplicación Formulario de las calificaciones de alumnos de secundaria. --Java con Apache-Netbeans--

Este proyecto está basado en elaborar una aplicación-formulario mediante el Netbeans, programado con Java, que permita rellenar los datos de cada alumno.

El proyecto con una programación orientada a objetos, está dividido en diferentes clases: 

- Main: Se inicia al ejecutar el programa, generando un objeto de tipo vista (el formulario).
- Vista: Tendrá todo el código que genera la interfaz de la aplicación, la parte visual, además se generarán los avisos con las restricciones       mencionadas
- Persona : Comprende todos los atributos del alumno:
                                                        - Id
                                                        - Nombre
                                                        - Apellido
                                                        - Calificaciones
                                                        - Nota máxima
                                                        - Nota mínina
                                                        - Media aritmetica
                                                        - Media ponderada.
            
- MediaA : Se genera un hilo que calcula la media aritmética de cada alumno.

### Implementaciones

Se genera un Jframe con swing de Netbeans, en el que se seguirán las restricciones, sino se activará un aviso generado cada vez que se incumplan las condiciones establecidas. Para una mejor comprensión, se redondea los resultados de los cálculos a 2 decimales y quedarán los números centrados dentro de cada celda.

Para ello se establece lo siguiente:

1.	Cada alumno dispondrá de un campo de registro para su  Id, Nombre y Apellido y calificaciones de las 5 asignaturas : Matemáticas, Física, Lengua, Inglés e Historia.

<img src="imagenes/inicial.png" alt="Logo" width="500" height="500">

2.	Todos los atributos de los usuarios son obligatorios y no deben ser nulos o vacíos.

<img src="imagenes/3.png" alt="Logo" width="460" height="300">

3.	Hay asignaturas con diferente peso: Matematicas = 35 %, Fisica = 25 %, Lengua = 10%, Ingles = 25%, Historia = 5%

4.	Es obligatorio tener nota en todas las asignaturas. 

<img src="imagenes/3.png" alt="Logo" width="460" height="300">

5.	La nota mínima será un 1. Si se introduce un 0 en alguna asignatura, debería lanzarse una excepción.
    Esto queda solucionado con el aviso y restricción que sólo se pueda introducir valores comprendidos entre 10 y 100 en el siguiente punto.

6.	Las calificaciones para cada asignatura deben de estar entre 10 y 100. Si una calificación es menor que 10 utilizará una excepción y mostrará el mensaje «las calificaciones deben de estar entre 10 y 100».

<img src="imagenes/4.png" alt="Logo" width="460" height="300">

7.	Si la nota media de un alumno es menor 50, se resaltará en rojo la celda. Si es mayor tendrá un color azul.

<img src="imagenes/5.png" alt="Logo" width="460" height="100">

8.  Las casillas del formulario que reciban números, no podrán recibir caracteres  y viversa. 

<img src="imagenes/6.png" alt="Logo" width="460" height="200"> <img src="imagenes/7.png" alt="Logo" width="460" height="200">


Las notas por debajo de 50 serán de un color diferentes a aquellas que estén por encima de 50.
La aplicación tendrá además la posibilidad de eliminar y modificar cualquiera de las filas que se haya introducido.
En la edición, además se añade una restricción diferente, ya que si se quiere introducir un caracter en las casillas destinadas a números, no se escribirá nada.

<img src="imagenes/antes.png" alt="Logo" width="500" height="500"><img src="imagenes/despues.png" alt="Logo" width="500" height="500">


