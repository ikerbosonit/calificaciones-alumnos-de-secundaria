
/**
 * Clase que se inicia al ejecutar el programa, generando un objeto de tipo vista (el formulario),
 *  y se muestra. A partir de ese momento la clase vista se encargará de todas las ejecuciones
 * @author iker.garcia
 */
public class Main {
    
    public static void main(String[] args) {
        
        Vista vista=new Vista();
        vista.setVisible(true);
    }
    
}
